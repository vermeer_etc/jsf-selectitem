/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2016 Yamashita,Takahiro
 */
package com.mycompany.samplejsf.infrastructure.jsf.uicomponet;

import com.mycompany.samplejsf.infrastructure.resourse.CustomControl;
import java.util.Locale;
import java.util.ResourceBundle;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * JSFで参照するResourceBundleを生成するFactoryクラス.<br>
 * 本クラスで生成したクラスを{@literal faces-config.xml}に設定してJSFから参照する.<br>
 *
 *
 *
 * @author Yamashita,Takahiro
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class JsfResourceBundle {

    /**
     * ResourceBundleを生成する.<br>
     * 取得方法の制御については、CustomControlで指定をする.<br>
     *
     * @see com.mycompany.samplejsf.infrastructure.resourse.CustomControl
     *
     * @param bundleBaseName 取得対象のResourceBundleのBaseName（AAAA.propertiesのAAAAの部分）
     * @param customControl 必要情報を設定したCustomControl
     * @return 生成したResourceBundle
     */
    public static ResourceBundle getBundle(String bundleBaseName, CustomControl customControl) {
        Locale locale = JsfContextUtil.getLocale();
        return ResourceBundle.getBundle(bundleBaseName, locale, customControl);
    }

    /**
     * ResourceBundleを生成する.<br>
     * Controlを未指定の場合は、文字コードはデフォルト（ascii）、取得対象はpropertiesファイルのみとしてResourceBundleを生成する.<br>
     * 取得対象をpropertiesのみとするのは、検索対象を絞る事で性能改善を図る.
     *
     * @param bundleBaseName 取得対象のResourceBundleのBaseName（AAAA.propertiesのAAAAの部分）
     * @return 生成したResourceBundle
     */
    public static ResourceBundle getBundle(String bundleBaseName) {
        CustomControl customControl = CustomControl.builder()
                .formats(CustomControl.FORMAT_PROPERTIES)
                .build();
        return JsfResourceBundle.getBundle(bundleBaseName, customControl);
    }
}
