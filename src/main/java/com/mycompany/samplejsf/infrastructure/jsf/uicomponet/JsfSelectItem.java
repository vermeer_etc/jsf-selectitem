/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2016 Yamashita,Takahiro
 */
package com.mycompany.samplejsf.infrastructure.jsf.uicomponet;

import com.mycompany.samplejsf.domain.type.common.enumutil.EnumCodePropertyInterface;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.model.SelectItem;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;

/**
 * Jsfで使用するSelectItemを共通操作するためにラップしたpart
 *
 * @author Yamashita,Takahiro
 */
@RequiredArgsConstructor(staticName = "of") @Value
public class JsfSelectItem {

    /* SelectItemのリスト */
    private final List<SelectItem> values;

    /**
     * Enumクラスの内部フィールドからSelectItemを作成し、Jsfで使用するSelectItemを共通操作するためのpartクラスを生成する
     *
     * @param <E> Enumクラスの型
     * @param enumClass EnumのClass ex)Enum.class
     * @return Jsfで使用するSelectItemを共通操作するためのpartクラス
     */
    public static <E extends Enum<E> & EnumCodePropertyInterface> JsfSelectItem of(@NonNull Class<E> enumClass) {
        List<SelectItem> items = new ArrayList<>();
        ResourceBundle bundle = ResourceBundle.getBundle("message");
        for (E item : enumClass.getEnumConstants()) {
            String value = bundle.containsKey(item.getProperty())
                           ? bundle.getString(item.getProperty())
                           : item.getProperty();
            SelectItem selectItem = new SelectItem(item.getCode(), value);
            items.add(selectItem);
        }
        return JsfSelectItem.of(items);
    }

    /**
     * MapからSelectItemを作成し、Jsfで使用するSelectItemを共通操作するためのpartクラスを生成する
     *
     * @param itemMap Map＜key=型制限なし, value=message.propertiesの変換キーとなるpropertyKeyの文字列＞
     * @return Jsfで使用するSelectItemを共通操作するためのpartクラス
     */
    public static JsfSelectItem of(@NonNull Map<?, String> itemMap) {
        List<SelectItem> items = new ArrayList<>();
        itemMap.entrySet().stream()
                .forEachOrdered(map -> {
                    SelectItem item = new SelectItem(map.getKey(), map.getValue());
                    items.add(item);
                });
        return JsfSelectItem.of(items);
    }
}
