/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2016 Yamashita,Takahiro
 */
package com.mycompany.samplejsf.infrastructure.jsf.uicomponet;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.component.UIInput;
import javax.faces.event.AjaxBehaviorEvent;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

/**
 * AjaxのEventObjectから情報取得を行う
 *
 * @author Yamashita,Takahiro
 */
@RequiredArgsConstructor(staticName = "of") @EqualsAndHashCode
public class JsfAjaxBehaviorEvent {

    private final AjaxBehaviorEvent event;

    /**
     * Ajaxの操作対象となったEventオブジェクトから取得した値を返却する. <br>
     * 取得結果は呼出側でキャストを行う事
     *
     * @return eventから取得した値
     */
    public Object getValue() {
        UIInput uiInput = (UIInput) this.event.getComponent();
        return uiInput.getValue();
    }

    /**
     * Ajaxの操作対象となったui:repeatの該当行のIndexを取得する.<br>
     *
     * @return eventが発生した該当行位置のindex
     */
    public int uiRepeatRowIndex() {
        String clientId = this.event.getComponent().getParent().getClientId();
        Pattern pat = Pattern.compile("[0-9]*$");
        Matcher mat = pat.matcher(clientId);
        if (mat.find() == false) {
            throw new IllegalArgumentException("this method must call ui:repeat ajax event");
        }
        return Integer.parseInt(mat.group());
    }
}
