/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2016 Yamashita,Takahiro
 */
package com.mycompany.samplejsf.infrastructure.jsf.uicomponet;

import java.util.Locale;
import javax.faces.context.FacesContext;
import lombok.NonNull;

/**
 * FacesContextを使用するユーティリティクラス
 *
 * @author Yamashita,Takahiro
 */
public class JsfContextUtil {

    private JsfContextUtil() {
    }

    /**
     * JSFのid値のセパレーター文字列を取得する
     *
     * @return セパレーター文字列
     */
    public static String getNamingContainerSeparatorChar() {
        return String.valueOf(FacesContext.getCurrentInstance().getNamingContainerSeparatorChar());
    }

    /**
     * ui:repeatの領域の外部からrenderで更新対象とするidの文字列を編集する.<br>
     * 編集後に生成されるトリガー側のajaxタブ<br>
     * <br>
     * 呼出実装例. <br>
     *
     * {@code JsfContextUtil.toRepaetTargetRender("target", 2, "root", "rep");}<br>
     *
     * 生成されたJSFのHTML <br>
     *
     * {@literal <f:ajax *** render="root-rep-0-target root-rep-1-target root-rep-2-target">} <br>
     *
     * <pre>
     * {@literal <form jsfc="h:form" id="root">}<br>
     * ・・・
     * {@literal <tr jsfc="ui:repeat" value="#{***}" var="row" id="rep">}
     * {@literal   <td>}
     * {@literal     <input type="checkbox" id="target" jsfc="h:selectBooleanCheckbox"  value="#{***}" />}
     * {@literal   </td>}
     * </pre>
     *
     * rootからrepまでの間に階層があれば、その階層のidを接合対象とする.<br>
     * taeger = 更新対象の行のid.<br>
     * <pre>
     * {@literal <td>}<br>
     * {@literal   <input type="checkbox" id="target" jsfc="h:selectBooleanCheckbox"  value="#{row.isSelect}" />}<br>
     * {@literal </td>}<br>
     * </pre>
     *
     * @param targetAttrId 生成対象のxhtmlのid値
     * @param maxIndex 繰り返しindexの最大値
     * @param prefixAttrsIds 生成対象のxhtmlのルートからのid値.
     * @return index=0から指定した最大indexまでのJSF表記の繰り返しid表記
     */
    public static String toRepaetTargetRender(@NonNull String targetAttrId, @NonNull Integer maxIndex, @NonNull String... prefixAttrsIds) {
        Integer prefixAttrsLength = prefixAttrsIds.length;
        String separator = JsfContextUtil.getNamingContainerSeparatorChar();
        String[] renderAttrIds = new String[maxIndex + 1];
        for (int i = 0; i <= maxIndex; i++) {
            String[] render = new String[prefixAttrsLength + 2];
            System.arraycopy(prefixAttrsIds, 0, render, 0, prefixAttrsLength);
            render[prefixAttrsLength] = String.valueOf(i);
            render[prefixAttrsLength + 1] = targetAttrId;
            String renderAttr = String.join(separator, render);
            renderAttrIds[i] = renderAttr;
        }
        return String.join(" ", renderAttrIds);
    }

    /**
     * 接続しているユーザーのロケールを取得する.
     *
     * @return 接続しているユーザーのロケール
     */
    public static Locale getLocale() {
        return FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }
}
