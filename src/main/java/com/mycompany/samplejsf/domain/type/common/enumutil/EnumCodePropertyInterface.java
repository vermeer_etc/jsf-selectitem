package com.mycompany.samplejsf.domain.type.common.enumutil;

/**
 * propertiesの仕様を前提として拡張をしたEnumクラスを共通操作するためのInterface
 *
 * @author Yamashita,Takahiro
 */
public interface EnumCodePropertyInterface {

    /**
     * code値の取得
     *
     * @return
     */
    public Object getCode();

    /**
     * propertyKeyの取得
     *
     * @return
     */
    public String getProperty();

}
