package com.mycompany.samplejsf.domain.type;

import com.mycompany.samplejsf.domain.type.common.enumutil.EnumCodePropertyInterface;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor @Getter
public enum Gender implements EnumCodePropertyInterface {
    MALE(1, "gender_male"), FEMALE(2, "gender_female");

    private final Integer code;
    private final String property;

}
