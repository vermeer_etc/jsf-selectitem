package com.mycompany.samplejsf.domain.type.common.enumutil;

import lombok.NonNull;

/**
 * propertiesから値を取得することを前提として拡張をしたEnumのFactory
 *
 * @author Yamashita,Takahiro
 */
public class EnumCodeProperty {

    /**
     * properties用に拡張したEnumのcode値から取得した拡張Enumを生成する
     *
     * @param <E> EnumCodePropertyInterfaceで拡張したEnumClass
     * @param enumType 生成対象となるEnumClassの型となるClass
     * @param code テーブルや定数として指定しているcode値
     * @return codeに一致するEnumクラス
     */
    public static <E extends Enum<E> & EnumCodePropertyInterface> E codeOf(Class<E> enumType, @NonNull Object code) {
        for (E type : enumType.getEnumConstants()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     * properties用に拡張したEnumのpropertyKey値から取得した拡張Enumを生成する
     *
     * @param <E> EnumCodePropertyInterfaceで拡張したEnumClass
     * @param enumType 生成対象となるEnumClassの型となるClass
     * @param property propertiesの検索に使用するpropertyKey
     * @return propertyKeyに一致するEnumクラス
     */
    public static <E extends Enum<E> & EnumCodePropertyInterface> E propertyOf(Class<E> enumType, @NonNull String property) {
        for (E type : enumType.getEnumConstants()) {
            if (type.getProperty().equals(property)) {
                return type;
            }
        }
        throw new IllegalArgumentException();
    }
}
