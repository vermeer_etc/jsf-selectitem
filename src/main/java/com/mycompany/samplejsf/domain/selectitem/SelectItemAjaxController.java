package com.mycompany.samplejsf.domain.selectitem;

import com.mycompany.samplejsf.domain.type.Gender;
import com.mycompany.samplejsf.domain.type.common.enumutil.EnumCodeProperty;
import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfAjaxBehaviorEvent;
import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfSelectItem;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Named(value = "selectItemAjax")
@SessionScoped
@NoArgsConstructor @Getter
public class SelectItemAjaxController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Setter
    private Integer selectItemValueEnumCodeProperty;

    @Setter
    private Integer selectItemValueMap;

    private JsfSelectItem enumCodeProperty;
    private JsfSelectItem selectItemMap;

    private Map<Integer, String> dummuyMap;

    @PostConstruct
    public void init() {
        this.selectItemValueEnumCodeProperty = Gender.FEMALE.getCode();
        this.enumCodeProperty = JsfSelectItem.of(Gender.class);
        this.replaceSelectItemMap();
        this.printLog("init");
    }

    public void submit() {
        this.replaceSelectItemMap();
        this.printLog("submit");
    }

    public void changeItem(AjaxBehaviorEvent event) {
        this.selectItemValueMap = (Integer) JsfAjaxBehaviorEvent.of(event).getValue();
        this.replaceSelectItemMap();
        this.printLog("changeItem");
    }

    private void replaceSelectItemMap() {
        try {
            switch (EnumCodeProperty.codeOf(Gender.class, this.selectItemValueEnumCodeProperty)) {
                case MALE:
                    this.maleSnack();
                    break;
                case FEMALE:
                    this.femaleSnack();
                    break;
                default:
                    this.noSnack();
            }
        } catch (NullPointerException | IllegalArgumentException ex) {
            this.noSnack();
        }
    }

    private void maleSnack() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(1, "大福");
        map.put(2, "おはぎ");
        map.put(3, "みたらしだんご");
        map.put(4, "せんべい");
        this.selectItemValueMap = map.keySet().iterator().next();
        this.selectItemMap = JsfSelectItem.of(map);
        this.dummuyMap = map;
    }

    private void femaleSnack() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(5, "チョコ");
        map.put(6, "クッキー");
        map.put(7, "プリン");
        map.put(8, "ゼリー");
        this.selectItemValueMap = map.keySet().iterator().next();
        this.selectItemMap = JsfSelectItem.of(map);
        this.dummuyMap = map;
    }

    private void noSnack() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(0, "");
        this.selectItemValueMap = map.keySet().iterator().next();
        this.selectItemMap = JsfSelectItem.of(map);
        this.dummuyMap = map;
    }

    private void printLog(String label) {
        System.out.println("label = " + label);
        System.out.println("selectItemValueEnumCodeProperty = " + selectItemValueEnumCodeProperty);
        if (selectItemValueEnumCodeProperty != null) {
            System.out.println("selectItemValueEnumCodeProperty codeOf = " + EnumCodeProperty.codeOf(Gender.class, selectItemValueEnumCodeProperty));
        }
        System.out.println("selectItemValueMap = " + this.selectItemValueMap);
        System.out.println("selectItemValueMap value = " + this.dummuyMap.get(this.selectItemValueMap));
    }
}
