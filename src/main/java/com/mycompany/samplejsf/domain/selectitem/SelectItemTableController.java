package com.mycompany.samplejsf.domain.selectitem;

import com.mycompany.samplejsf.domain.type.Gender;
import com.mycompany.samplejsf.domain.type.common.enumutil.EnumCodeProperty;
import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfAjaxBehaviorEvent;
import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfSelectItem;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Named(value = "selectItemTableAjax")
@SessionScoped
@NoArgsConstructor
public class SelectItemTableController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter @Setter
    private ItemForms genderSnackTable;

    private Map<Gender, Map<Integer, String>> genderShackMap;

    @PostConstruct
    public void init() {
        Map<Gender, Map<Integer, String>> enumMap = new EnumMap<>(Gender.class);
        Map<Integer, String> maleSnack = this.maleSnack();
        enumMap.put(Gender.MALE, maleSnack);
        Map<Integer, String> femaleSnack = this.femaleSnack();
        enumMap.put(Gender.FEMALE, femaleSnack);
        this.genderShackMap = enumMap;

        Integer defaultMaleSnackCode = this.genderShackMap.get(Gender.MALE).entrySet().iterator().next().getKey();
        Integer defaultFemaleSnackCode = this.genderShackMap.get(Gender.FEMALE).entrySet().iterator().next().getKey();
        this.genderSnackTable = ItemForms.builder()
                .form(ItemForm.builder()
                        .seq(1)
                        .genderCode(Gender.MALE.getCode())
                        .genderEnum(JsfSelectItem.of(Gender.class))
                        .snackCode(defaultMaleSnackCode)
                        .snackList(JsfSelectItem.of(this.genderShackMap.get(Gender.MALE)))
                        .build())
                .form(ItemForm.builder()
                        .seq(2)
                        .genderCode(Gender.FEMALE.getCode())
                        .genderEnum(JsfSelectItem.of(Gender.class))
                        .snackCode(defaultFemaleSnackCode)
                        .snackList(JsfSelectItem.of(this.genderShackMap.get(Gender.FEMALE)))
                        .build())
                .build();
        this.printLog("init");
    }

    public void submit() {
        this.printLog("submit");
    }

    public void changeRowGender(AjaxBehaviorEvent event) {
        int index = JsfAjaxBehaviorEvent.of(event).uiRepeatRowIndex();
        ItemForm form = genderSnackTable.get(index);
        Gender gender = EnumCodeProperty.codeOf(Gender.class, form.getGenderCode());
        form.setSnackCode(this.genderShackMap.get(gender).entrySet().iterator().next().getKey());
        form.setSnackList(JsfSelectItem.of(this.genderShackMap.get(gender)));
        this.printLog("ajax " + index);
    }

    private Map<Integer, String> maleSnack() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(1, "大福");
        map.put(2, "おはぎ");
        map.put(3, "みたらしだんご");
        map.put(4, "せんべい");
        return map;
    }

    private Map<Integer, String> femaleSnack() {
        Map<Integer, String> map = new LinkedHashMap<>();
        map.put(5, "チョコ");
        map.put(6, "クッキー");
        map.put(7, "プリン");
        map.put(8, "ゼリー");
        return map;
    }

    private void printLog(String label) {
        System.out.println("label = " + label);

        this.getGenderSnackTable().getForms().stream()
                .forEachOrdered(form -> {
                    Gender gender = EnumCodeProperty.codeOf(Gender.class, form.getGenderCode());
                    String snackName = this.genderShackMap.get(gender).get(form.getSnackCode());

                    System.out.println("SEQ:" + form.getSeq() + ":GenderCode:" + form.getGenderCode()
                                       + ":GenderValue:" + gender.getProperty() + ":SnackCode:" + form.getSnackCode() + ":SnackName:" + snackName);

                });
    }
}
