package com.mycompany.samplejsf.domain.selectitem.selectform;

import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfAjaxBehaviorEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Named(value = "selectItemTableRowSelect")
@SessionScoped
@NoArgsConstructor
public class SelectItemTableRowSelectController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter @Setter
    private SelectForms table;

    @PostConstruct
    public void init() {
        List<SelectForm> forms = new ArrayList<>();
        for (int i = 0; i < 28; i++) {
            SelectForm form = SelectForm.builder().isSelect(false).seq(i + 1).build();
            forms.add(form);
        }

        this.table = SelectForms.of(forms);
        this.printLog("init");
    }

    public void submit() {
        this.printLog("submit");
    }

    public void checkAllSelect(AjaxBehaviorEvent event) {
        this.table.setFormsIsSelect((Boolean) JsfAjaxBehaviorEvent.of(event).getValue());
        this.printLog("ajax all select");
    }

    public void changePagePos(AjaxBehaviorEvent event) {
        this.table.replaceAllSelectRenderAttrIds((Integer) JsfAjaxBehaviorEvent.of(event).getValue());
        this.printLog("ajax change page position");
    }

    public void checkDetailRowSelect(AjaxBehaviorEvent event) {
        this.table.setFormIsSelect(JsfAjaxBehaviorEvent.of(event));
        this.printLog("ajax " + JsfAjaxBehaviorEvent.of(event).getValue());
    }

    private void printLog(String label) {
        System.out.println("label = " + label + "::allSelect:" + table.getIsSelect());
        this.getTable().getForms().stream()
                .forEachOrdered(form -> {
                    System.out.println(":rowSelect:" + form.getIsSelect() + ":seq:" + form.getSeq());
                });
    }
}
