package com.mycompany.samplejsf.domain.selectitem.selectform;

import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfAjaxBehaviorEvent;
import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfContextUtil;
import java.util.List;
import java.util.Objects;
import lombok.Data;

@Data
public class SelectForms {

    private static final int MAX_ROW_COUNT = 10;
    private Integer startViewPagePos;
    private String allSelectRenderAttrIds;

    private Boolean isSelect;

    private List<SelectForm> forms;

    private SelectForms() {
    }

    public static SelectForms of(List<SelectForm> forms) {
        SelectForms selectForms = new SelectForms();
        selectForms.setIsSelect(false);
        selectForms.setForms(forms);
        selectForms.replaceAllSelectRenderAttrIds(1);
        return selectForms;
    }

    public List<SelectForm> getViewForms() {
        List<SelectForm> viewForms;
        Integer startIndex = (this.startViewPagePos - 1) * MAX_ROW_COUNT;
        Integer endIndex = startIndex + toMaxIndex(this.startViewPagePos) + 1;
        viewForms = this.forms.subList(startIndex, endIndex);
        return viewForms;
    }

    public void setFormsIsSelect(Boolean isSelect) {
        this.isSelect = isSelect;
        this.forms.stream().forEach(form -> {
            form.setIsSelect(isSelect);
        });
    }

    public void setFormIsSelect(JsfAjaxBehaviorEvent jsfEvent) {
        this.isSelect = false;
        this.getForms().get(jsfEvent.uiRepeatRowIndex()).setIsSelect((Boolean) jsfEvent.getValue());
    }

    public void replaceAllSelectRenderAttrIds(Integer pagePos) {
        this.startViewPagePos = pagePos;
        this.allSelectRenderAttrIds = JsfContextUtil.toRepaetTargetRender("isselect", this.toMaxIndex(pagePos), "f", "rep");
    }

    private Integer toMaxIndex(Integer pagePos) {
        Integer maxPageCnt = this.forms.size() / MAX_ROW_COUNT;
        Integer rowCountMod = this.forms.size() % MAX_ROW_COUNT;
        maxPageCnt = rowCountMod < (MAX_ROW_COUNT - 1) ? maxPageCnt + 1 : maxPageCnt;
        return Objects.equals(pagePos, maxPageCnt) ? rowCountMod - 1 : MAX_ROW_COUNT - 1;
    }

}
