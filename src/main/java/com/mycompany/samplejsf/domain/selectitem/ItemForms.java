package com.mycompany.samplejsf.domain.selectitem;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

@Builder @Data
public class ItemForms {

    @Singular
    private List<ItemForm> forms;

    public ItemForm get(int rowIndex) {
        return this.forms.get(rowIndex);
    }

}
