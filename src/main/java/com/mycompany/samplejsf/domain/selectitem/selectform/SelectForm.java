package com.mycompany.samplejsf.domain.selectitem.selectform;

import lombok.Builder;
import lombok.Data;

@Builder @Data
public class SelectForm {

    private Boolean isSelect;
    private Integer seq;

}
