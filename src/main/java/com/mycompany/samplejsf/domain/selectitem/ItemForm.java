package com.mycompany.samplejsf.domain.selectitem;

import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfSelectItem;
import lombok.Builder;
import lombok.Data;

@Builder @Data
public class ItemForm {

    private Integer seq;
    private Integer genderCode;
    private JsfSelectItem genderEnum;

    private Integer snackCode;
    private JsfSelectItem snackList;

}
