package com.mycompany.samplejsf.domain.index;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import lombok.NoArgsConstructor;

@Named(value = "root")
@RequestScoped
@NoArgsConstructor
public class indexController {

    public String fwSelectItem() {
        return "selectItem.xhtml";
    }

    public String fwSelectItemAjax() {
        return "selectItemAjax";
    }

    public String fwSelectItemTable() {
        return "selectItemTable";
    }

    public String fwSelectItemTableRowSelect() {
        return "selectItemTableRowSelect";
    }

    public String fwLabel() {
        return "label";
    }
}
