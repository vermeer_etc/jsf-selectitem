/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2016 Yamashita,Takahiro
 */
package com.mycompany.samplejsf.domain.resource;

import com.mycompany.samplejsf.infrastructure.jsf.uicomponet.JsfResourceBundle;
import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * 表示ラベルResourceBundle
 *
 * @author Yamashita,Takahiro
 */
public class LabelResourceBundle extends ResourceBundle {

    /**
     * {@inheritDoc}
     */
    public LabelResourceBundle() {
        ResourceBundle bundle = JsfResourceBundle.getBundle("label");
        super.setParent(bundle);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object handleGetObject(String key) {
        return super.parent.getObject(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getKeys() {
        return super.parent.getKeys();
    }
}
